#!/usr/bin/python

import pandas as pd

d1 = pd.read_csv('1.csv') #e.g. meat 
d2 = pd.read_csv('2.csv') #e.g. veg

d3 = d1.append(d2)

for x in range(1961,2012):
	d = d3[d3['Year']==x]
	d.to_csv(str(x)+'.csv', sep='\t',index=False)
	print x