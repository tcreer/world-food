#!/usr/bin/python

import pandas as pd, difflib

#csv = pandas.read_csv('fao.csv',header=0)
#csv.columns
#csv[csv['AreaName']=='United Kingdom']

#Domain Code,Domain,AreaCode[2],AreaName,ElementCode,
#ElementName,ItemCode[6],ItemName[7],Year[8],Unit,Value[10],Flag,FlagD

def main():
	for x in range(1961,2012):
		replace_withiso('./data/'+ str(x)+'.csv')
 #filt(2011,'Grand Total')

def filt(year,foodtype):
	d_orig = pd.read_csv('fao.csv_')
	d = d_orig[(d_orig['Year']==year) &(d_orig['ItemName']==foodtype) ]
	d2 = d[['AreaName','Value']]
	d2.columns = ['id','value']
	d2.to_csv('out.tsv', sep='\t',index=False)




#pre processing
import csv

#fix to keep headers
def replace_withiso(filename):
	iso = open('iso3166.csv')
	csv_c = csv.reader(iso)
	c_code = []
	c_name = []
	for row in csv_c:
	  #print row
	  c_code.append(row[0])
	  c_name.append(row[1].upper())
	
	in_file = open(filename, "rb")
	reader = csv.reader(in_file, delimiter='\t')
	out_file = open(filename + "_", "wb")
	writer = csv.writer(out_file, delimiter='\t')
	row0 = 'Domain Code,Domain,AreaCode,AreaName,ElementCode,ElementName,ItemCode,ItemName,Year,Unit,Flag,FlagD'.split(',')
	writer.writerow(row0)
	next(reader, None)
	flag=0
	for row in reader:
	    try:
	     c_n = row[3].upper()
	    except:
	     if flag == 0:
	       print row
	       flag = 1
	    if int(float(row[2])) > 1000: #not a country
	     continue
	    if row[3].strip() == 'USSR':
	     row[2] = 'ru'
	     ussr = ['am','az','ee','ge','kz','kg','lv','lt','md','tj','tm','uz','ru','ua','by']
	     for u in ussr:
	      rowcp = row
	      rowcp[2] = u
	      writer.writerow(rowcp)
	     writer.writerow(row)
	     continue
	    if row[3].strip() == 'Yugoslav SFR':
	     row[2] = 'rs'
	     ussr = ['me','rs']
	     for u in ussr:
	      rowcp = row
	      rowcp[2] = u
	      writer.writerow(rowcp)
	     writer.writerow(row)
	     continue	     
	    
	    close = difflib.get_close_matches(c_n,c_name,1)
	    try:
	      row[2] = c_code[c_name.index(close[0])].lower()
	      flag = 0 
	    except: #print out cases cant find, can do manually
	      if flag == 0:
	       print c_n
	       flag = 1
	       
	    writer.writerow(row)
	    
	in_file.close()    
	out_file.close()
	
#replace_withiso('fao.csv')

## yugo, 248.0	Yugoslav SFR, divided into
#me & rs

## USSR, divided into: http://en.wikipedia.org/wiki/ISO_3166-3   228.0	USSR
#~ Divided into: [note 6]
#~ Armenia (AM, ARM, 051)
#~ Azerbaijan (AZ, AZE, 031)
#~ Estonia (EE, EST, 233)
#~ Georgia (GE, GEO, 268)
#~ Kazakhstan (KZ, KAZ, 398)
#~ Kyrgyzstan (KG, KGZ, 417)
#~ Latvia (LV, LVA, 428)
#~ Lithuania (LT, LTU, 440)
#~ Moldova, Republic of (MD, MDA, 498)
#~ Russian Federation (RU, RUS, 643)
#~ Tajikistan (TJ, TJK, 762)
#~ Turkmenistan (TM, TKM, 795)
#~ Uzbekistan (UZ, UZB, 860)


if __name__ == '__main__':
    main()

