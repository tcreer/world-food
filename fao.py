#!/usr/bin/python

from pandas import *
from ipyD3 import *
from numpy import array

df = read_csv("out.tsv",'\t')

d3 = d3object(width=500,
height=300,
style='JFFigure',
number=1,
title='Grand Total',
desc='2011')

d3.addJs('''
        var svg = d3.select("#"+d3ObjId)
                    .append("svg")
                    .attr("width", width)
                    .attr("height", height)
                    .append("g")
                    .attr("id", "#"+d3ObjId+'InnerG')

        // Convert 2d array to dictionary PostalState -> percent
        var umap = []
        data.map(function(d) {umap[d[1]]=Number(d[2])})
        var v = Object.keys(umap).map(function(k){return umap[k]})

        // Color bar
        // Adapted from http://tributary.io/tributary/3650755/
        var colorScale = d3.scale.linear()
                           .domain(d3.extent(v))
                           .interpolate(d3.interpolateHcl)
                           .range(["blue", "red"]);

        svg.selectAll("rect")
           .data(d3.range(d3.min(v), d3.max(v), (d3.max(v)-d3.min(v))/50.0))
           .enter()
           .append("rect")
           .attr({width: 25,
                  height: 5,
                  y: function(d,i) { return height-25-i*5 },
                  x: width-50,
                  fill: function(d,i) { return colorScale(d); } })

        // Scale for color bar
        svg.append("g")
           .attr("transform", "translate(" + (width-25) + "," + (height-25-5*49) + ")")
           .call(d3.svg.axis()
                   .scale(d3.scale.linear().domain(d3.extent(v)).range([5*50,0]))
                   .orient("right"));

        // Load U.S. map SVG from Wikipedia
        d3.xml("http://upload.wikimedia.org/wikipedia/commons/0/03/BlankMap-World6.svg",
               "image/svg+xml", function(xml) {

            svg.append("g").attr("transform", "scale(" + (width/1200.0) + ")")
               .node().appendChild(document.importNode(xml.documentElement, true));

            // Cheat.  No easy way to bind data based upon pre-existing DOM ids
            // so just "loop" with each() and style/fill directly.
            d3.selectAll(".landxx").each(function() {
                d3.select(this).style("fill",colorScale(umap[this.id]))
            })
        });
''')

d3.addVar(data=array(df.to_records().tolist()))


html=d3.render(mode=('show','html'))
with open('f-out.html','w') as f:
	f.write(html.data)
	f.close()