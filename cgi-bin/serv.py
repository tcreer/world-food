#!/usr/bin/env python
#if __name__ == "__main__":

import pandas as pd, difflib
import cgi, cgitb,os.path
import cStringIO
form = cgi.FieldStorage() 

def sanitise(input):
	o = input.replace('/','')
	o = o.replace(';','')
	o = o.replace('\\','')
	o = o.replace('$','')
	o = o.replace('~','')
	return o

old = ['Vegetables','Animal products','Milk','Fish','Fruits','Cereals','Rice','Sugar and Sweeteners','Beef','Pork','Chicken','Sheep or Goat','Alcohol']
new = ['Vegetal Products','Animal Products','Milk - Excluding Butter','Fish, Seafood','Fruits - Excluding Wine','Cereals - Excluding Beer',
'Rice (Milled Equivalent)','sugar','Bovine Meat','Pigmeat','Poultry Meat','mutton','Alcoholic Beverages']

def swapterms(input):
	for i, val in enumerate(old):
		if val.strip() == input:
			return new[i]
	return input

#year=0
#absolute=0
#foodtype=''
try:
	year = int(form.getvalue('year'))

	foodtype  = swapterms(sanitise(form.getvalue('food')))
	if foodtype == 'mutton':
		foodtype = "Mutton & Goat Meat"
	if foodtype == 'sugar':
		foodtype = "Sugar & Sweeteners"
	absolute  = int(form.getvalue('abs'))
except:
	year = 2011
	foodtype = 'Grand Total'
	absolute = 1


output = cStringIO.StringIO()

#fname = foodtype + str(year) + str(absolute) + '.tsv'

#if os.path.isfile('/home/tom/projects/fo/' + fname):
#	print 'y'
#else:

d_orig = pd.read_csv('../data/'+str(year)+'.csv_',sep='\t')
#d_orig = pd.read_csv('/home/tom/projects/fo/fao.csv_',sep=',')

if absolute == 0:
	d = d_orig[(d_orig['ItemName']==foodtype) ]
	d2 = d[['AreaCode','AreaName','Unit']]
	d2.columns = ['code','name','Calories (kCal)']
	#d2.sort_index(by=['code'], ascending=[False])
	d2.to_csv(output, sep='\t',index=False)
elif absolute == 1:

	d_a = d_orig[(d_orig['ItemName']=='Grand Total') ]
	d_2a = d_a[['AreaName','Unit']]
	d_2a.columns = ['name','t_v']
	
	d = d_orig[(d_orig['ItemName']==foodtype) ]
	d2 = d[['AreaName','Unit']]
	d2.columns = ['name','Unit']
	
	d_c = d_a[['AreaCode','AreaName']]
	d_c.columns = ['code','name']
	
	merged = pd.merge(d2,d_2a,on="name")
	answer = 100*merged['Unit']/merged['t_v']

	df = pd.DataFrame(zip(merged['name'],answer))
	df.columns = ['name','Unit']
	d3 = pd.merge(d_c,df,on="name")
	
	d3.columns = ['code','name','Proportion']
	
	#2.sort_index(by=['code'], ascending=[False])
	d3.to_csv(output, sep='\t',index=False)
print "Content-Type: text/csv\n"
print output.getvalue().strip()

output.close()

#print fname
#with open('/home/tom/projects/fo/' + fname, 'r') as f:
#   print f.read()
#   f.close()


#Wheat and products
#Barley and products
#Maize and products
#Nuts and products
#Potatoes and products
#"Cereals, Other"
#"Beverages, Alcoholic"
#Fruits - Excluding Wine
#Alcoholic Beverages
#Grand Total

