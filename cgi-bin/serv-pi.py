#!/usr/bin/python

import pandas as pd, difflib
import cgi, cgitb,os.path
import cStringIO
form = cgi.FieldStorage() 

def sanitise(input):
	o = input.replace('/','')
	o = o.replace(';','')
	o = o.replace('\\','')
	o = o.replace('$','')
	o = o.replace('~','')
	return o

try:
	year = int(form.getvalue('year'))
	c  = sanitise(form.getvalue('country'))
except:
	year = 2011
	c = 'Armenia'

d_orig = pd.read_csv('../data/'+str(year)+'.csv_',sep='\t')

d = d_orig[(d_orig['Year']==year) &(d_orig['AreaName']==c) ]
if len(d.index) < 1:
	d = d_orig[(d_orig['Year']==year) &(d_orig['AreaCode']==c) ]

ftypes = """Cereals - Excluding Beer
Sugar & Sweeteners
Milk - Excluding Butter
Meat
Vegetable Oils
Vegetables
Fruits - Excluding Wine
Animal fats
Starchy Roots
Eggs
Eggs
Stimulants
Alcoholic Beverages
Offals
Treenuts
Oilcrops
Fish, Seafood
Honey
Miscellaneous
Spices
Aquatic Products, Other
Pulses
Aquatic Plants"""

f = ftypes.split('\n')

d2 = d[['ItemName','Unit']]

df = d2[d2['ItemName'].isin(f)]

df.columns = ['name','Unit']
df[['Unit']] = df[['Unit']].astype(int)
df = df.sort(['Unit'],ascending=[False])
df = df.drop_duplicates(['name'])

output = cStringIO.StringIO()
df.to_csv(output, sep='\t',index=False)
print "Content-Type: text/csv\n"
print output.getvalue().strip()
